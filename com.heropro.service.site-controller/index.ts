// HeroPro Site Controller
// (C) Cameron Fleming (Nevexo) 2019 - MIT License 

// Setup required dependencies
const Winston = require("winston") // Used for Logging (https://www.npmjs.com/package/winston)

// Setup Logging
const logger = Winston.createLogger({
    level: process.env.NODE_ENV || 'info',
    format: Winston.format.json(),
    defaultMeta: {},
    transports: [
      // - Write to all logs with level `info` and below to `combined.log` 
      // - Write all logs error (and below) to `error.log`.
      new Winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
      new Winston.transports.File({ filename: 'logs/combined.log' }),
    ]
  });
   
  // Log to console if not running in production
  if (process.env.NODE_ENV !== 'production') {
    logger.add(new Winston.transports.Console({
      format: Winston.format.simple()
   }));
}